
/**
 * Umsetzung der Klasse Knoten im Graphen
 * 
 * @author R. Löffler
 * @version 03.04.2019
 */
public class KnotenGraph
{
    // Attribute
    private String bezeichnung;    
    // Attribute Ende
    // -------------------------------------------------------------------------   
    // Konstruktoren
    public KnotenGraph(String bezeichnung){
        this.bezeichnung = bezeichnung;
    }
    // Konstruktoren Ende
    // -------------------------------------------------------------------------
    // Methoden
    public String bezeichnungGeben(){
        return this.bezeichnung;
    }

    public String getbezeichnung(){
        return this.bezeichnung;
    }

    public String bezForGeben(int laengePar){
        String bezeichnerLok = this.bezeichnung;
        if(bezeichnerLok.length() == laengePar){
            return this.bezeichnung;
        }
        else{
            if (bezeichnerLok.length() < laengePar) {
                while (bezeichnerLok.length() < laengePar) { 
                    bezeichnerLok += " ";
                } // end of while
            } 
            else{
                bezeichnerLok = this.bezeichnung.substring(0, laengePar);
            } // end of if-else
        }
        return bezeichnerLok;
    }

    public void test(){
        this.bezeichnung = "Tes";
        System.out.println("Erster Bezeichner Tes mit 3 Buchstaben");
        System.out.println(this.bezForGeben(5)+"Ende des Srtings"); 
        this.bezeichnung = "Teste";      
        System.out.println("Erster Bezeichner Teste mit genau 5 Buchstaben");
        System.out.println(this.bezForGeben(5));
        this.bezeichnung = "Testen";
        System.out.println("Erster Bezeichner Testen mit 6 Buchstaben");
        System.out.println(this.bezForGeben(5));
    }
    // Methoden Ende
    // -------------------------------------------------------------------------

}
