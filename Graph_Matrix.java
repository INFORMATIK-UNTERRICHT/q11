
/**
 * Diese Klasse repräsentiert den Graphen, die Kanten werden
 * mit 
 * ilfe einer sogenannten Adjazenzmatrix dargestellt.
 * @author R. Löffler 
 * @version 04.04.2019
 */
public class Graph_Matrix{
    // Attribute
    // Anfang Attribute
    private int anzahlKnoten;
    private KnotenGraph[] knoten;
    private int[][] matrix;
    private boolean[] besucht;
    // Ende Attribute
    // ------------------------------------------ 
    // Konstruktoren
    public Graph_Matrix(int anzahlKnotenPar) {
        this.anzahlKnoten = 0;
        this.knoten = new KnotenGraph[anzahlKnotenPar];
        this.matrix = new int[anzahlKnotenPar][anzahlKnotenPar];
        this.besucht = new boolean[anzahlKnotenPar];
        this.besuchtAufFalseSetzen();
    }
    // Anfang Methoden
    // Konstruktoren Ende
    // ------------------------------------------ 
    // Methoden
    public int knotenNummer(String knotenBezeichner){
        for (int i = 0; i < anzahlKnoten; i++ ) {
            if (knoten[i].getbezeichnung().equals(knotenBezeichner)) {
                return i; 
            } // end of if   
        } // end of for
        return -1;    
    }

    public int knotenAnzahlGeben(){
        return anzahlKnoten;
    }

    public int kanteGewichtGeben(String startPar, String zielPar){
        int startNummerLok = this.knotenNummer(startPar);
        int zielNummerLok = this.knotenNummer(zielPar);
        if (startNummerLok == -1 || zielNummerLok == -1) {
            System.err.print("Start- oder Zielbezeichnung nicht vorhanden");      
            return -1;
        } // end of if
        else{
            return matrix[startNummerLok][zielNummerLok];
        }
    }

    private boolean bezeichnerNochNichtVorhanden(String bezeichnerLok){
        for (int i = 0 ; i < this.anzahlKnoten ; i++ ) {
            if (this.knoten[i].getbezeichnung().equals(bezeichnerLok)) {
                return false;          
            }
        }
        return true;
    }

    public boolean knotenEinfuegen(String knotenBezeichnerPar){
        if (this.anzahlKnoten < this.knoten.length) {
            if (this.bezeichnerNochNichtVorhanden(knotenBezeichnerPar)) {
                this.knoten[this.anzahlKnoten] = new KnotenGraph(knotenBezeichnerPar);
                for (int i = 0 ; i < this.anzahlKnoten-1 ; i++) {
                    this.matrix[this.anzahlKnoten][i] = -1;
                    this.matrix[i][this.anzahlKnoten] = -1;
                }
                this.matrix[this.anzahlKnoten][this.anzahlKnoten] = 0;
                this.anzahlKnoten++;
            }
            return true;
        }
        else {
            return false;
        }
    }

    public boolean knotenEinfuegen(KnotenGraph knotenPar){
        if (this.anzahlKnoten < this.knoten.length) {
            if (this.bezeichnerNochNichtVorhanden(knotenPar.getbezeichnung())) {
                this.knoten[this.anzahlKnoten] = knotenPar;
                this.matrix[this.anzahlKnoten][this.anzahlKnoten] = 0;
                for (int i = 0 ; i < this.anzahlKnoten ; i++) {
                    this.matrix[this.anzahlKnoten][i] = -1;
                    this.matrix[i][this.anzahlKnoten] = -1;
                }
                this.anzahlKnoten++;
            }
            return true;
        }
        else {
            return false;
        }
    }

    public boolean kanteEinfuegen(String vonPar, String nachPar, int gewichtungPar){
        int startNummerLok = this.knotenNummer(vonPar);
        int zielNummerLok = this.knotenNummer(nachPar);
        if (startNummerLok != -1 
        && startNummerLok != zielNummerLok
        && zielNummerLok != -1) {
            this.matrix[startNummerLok][zielNummerLok] = gewichtungPar;
            this.matrix[zielNummerLok][startNummerLok] = gewichtungPar;
            return true;
        } 
        else {
            return false;
        }
    }

    public void ausgeben(int formatBreitePar){
        String zeileLok = "";
        // Kopfzeile der Ausgabematrix wird aufgebaut und
        // ausgegeben.
        for (int i = 0 ; i < formatBreitePar ; i++) {
            zeileLok = zeileLok.concat(" ");
        }
        // Aufbau der Ausgabe Zeile für Zeile
        for (int i = 0; i < this.anzahlKnoten; i++) {
            zeileLok = zeileLok.concat(this.knoten[i].bezForGeben(formatBreitePar));
        }
        System.out.println(zeileLok);
        // Rest der Matrix wird nun Zeile für Zeile aufgebaut
        for (int i = 0; i < this.anzahlKnoten; i++) {
            zeileLok = "";
            zeileLok = zeileLok.concat(this.knoten[i].bezForGeben(formatBreitePar));
            for (int j = 0; j < this.anzahlKnoten; j++) {
                zeileLok = zeileLok.concat(this.gewichtungForGeben(formatBreitePar, matrix[i][j]));
            }
            System.out.println(zeileLok);    
        }
    }

    public String gewichtungForGeben(int laengePar,int gewichtungPar){
        Integer gewichtungLok = gewichtungPar;
        String gewichtungStringLok = gewichtungLok.toString();
        if(gewichtungStringLok.length() == laengePar){
            return gewichtungStringLok;
        }
        else{
            if (gewichtungStringLok.length() < laengePar) {
                while (gewichtungStringLok.length() < laengePar) { 
                    gewichtungStringLok += " ";
                } // end of while
            } 
            else{
                gewichtungStringLok = gewichtungStringLok.substring(0, laengePar);
            } // end of if-else
        }
        return gewichtungStringLok;
    }

    private void besuchtAufFalseSetzen(){
        for ( int i = 0; i < this.besucht.length ; i++){
            this.besucht[i] = false;
        }
        System.out.println("Array besucht[] auf false gesetzt.");
    }

    private void besuchen(int knotenNummer){
        this.besucht[knotenNummer] = true;
        System.out.println(this.knoten[knotenNummer].bezeichnungGeben()+ " besucht;");
        for(int abzweigenummer = 0 ; abzweigenummer < this.anzahlKnoten ; abzweigenummer++){
            if((this.matrix[knotenNummer][abzweigenummer]>0) && (!this.besucht[abzweigenummer])){
                this.besuchen(abzweigenummer);
            }
        } 
        System.out.println("Der aktive Knoten mit der Bezeichnung " + 
            this.knoten[knotenNummer].bezeichnungGeben() + " ist fertig abgearbeitet.");
    }

    public void tiefensuche(String startKnotenPar){
        this.besuchtAufFalseSetzen();
        int startNummerLok = 0;
        startNummerLok = this.knotenNummer(startKnotenPar);
        if(startNummerLok != -1){
            this.besuchen(startNummerLok);
        }      
    }

    public void tiefensuche(int startKnotenNummerPar){
        this.besuchtAufFalseSetzen();
        if((startKnotenNummerPar >= 0)&&(startKnotenNummerPar<=this.anzahlKnoten)){
            this.besuchen(startKnotenNummerPar);
        }      
    }

    public void wegeSuchen(String startKnotenPar, String zielKnotenPar){
        int startNummerLok = this.knotenNummer(startKnotenPar);
        int zielNummerLok = this.knotenNummer(zielKnotenPar);
        // boolean Array besucht[] wird komplett auf false gesetzt.
        // Aber nur, wenn startKnotenName un d zielknotenName vorhanden.
        if(startNummerLok != -1 && zielNummerLok != -1 && startNummerLok != zielNummerLok){
            this.besuchtAufFalseSetzen();  
            // Ablaufen der Pfade wird gestartet
            this.ablaufen(startNummerLok, zielNummerLok, startKnotenPar, 0);
        }
        System.out.println("Wegesuche mit Startknoten " + startKnotenPar + " und Zielknoten " + zielKnotenPar + " abgeschlossen.");
    }

    public void wegeSuchen(int startKnotenPar, int zielKnotenPar){
        // boolean Array besucht[] wird komplett auf false gesetzt.
        // Aber nur, wenn startKnotenName un d zielknotenName vorhanden.
        if(startKnotenPar != -1 && zielKnotenPar != -1 && startKnotenPar != zielKnotenPar){
            this.besuchtAufFalseSetzen();  
            // Ablaufen der Pfade wird gestartet
            this.ablaufen(startKnotenPar, zielKnotenPar, this.knoten[startKnotenPar].getbezeichnung(), 0);
        }
        System.out.println("Wegesuche mit Startknoten " + this.knoten[startKnotenPar].getbezeichnung()
            + " und Zielknoten " + this.knoten[zielKnotenPar].getbezeichnung() + " abgeschlossen.");
    }

    public void ablaufen(int knotenNummerPar, int zielknotenNummerPar, String pfadPar, int laengePar){
        int neueLaengeLok = 0;
        String neuerPfadLok = null;

        // aktiven Knoten auf besucht setzen
        this.besucht[knotenNummerPar] = true;

        // Wenn der Zielknoten erreicht wurde, abbrechen und en aktuellen Pfad ausgeben
        if(knotenNummerPar == zielknotenNummerPar){
            System.out.println(pfadPar + " mit Länge: " + laengePar);
        }
        // Sionst werden alle existierenden noch möglichen Abzweigungen, wie bei Tiefensuche abgelaufen 
        else{
            // In der Matrix werden in der Zeile des aktiven Knoten alle Abzweigungen (Kanten vorhanden?)
            // abgelaufen
            for(int abzweigeNummer = 0 ; abzweigeNummer < this.anzahlKnoten ; abzweigeNummer++){
                // Gibt es eine Kante und dessen Zielknoten wurde noch nicht besucht?!
                if(this.matrix[knotenNummerPar][abzweigeNummer] > 0 && this.besucht[abzweigeNummer] == false){
                    // Verlängerung des Pfades in diese "Richtung"
                    neuerPfadLok = pfadPar + "/" + this.knoten[abzweigeNummer].bezeichnungGeben();
                    neueLaengeLok = laengePar + this.matrix[knotenNummerPar][abzweigeNummer];
                    // JETZT DER REKURSIVE AUFRUF DER METHODE ablaufen(....)
                    this.ablaufen(abzweigeNummer, zielknotenNummerPar , neuerPfadLok , neueLaengeLok);
                }
            }
        }
        // Hier, im Gegnsatz zur Tiefensuche, wierd der besuchte Knoten wieder "FREIGEGEBEN"
        this.besucht[knotenNummerPar] = false;
    }    

    public void kuerzesterWegDijkstra(String startKnotenPar, String zielKnotenPar, String letzterKonten){
        int startNummerLok = this.knotenNummer(startKnotenPar);
        int zielNummerLok = this.knotenNummer(zielKnotenPar);
        int [] distanzLok = new int[this.anzahlKnoten];
        this.besuchtAufFalseSetzen();
        for(int i = 0 ; i < this.anzahlKnoten ; i++){
            distanzLok[i] = 2147483647;
        }
        distanzLok[startNummerLok] = 0;

        while(nochNichtAlleBesucht()){
            
        }
    }

    private boolean nochNichtAlleBesucht(){
        for(int i = 0 ; i < this.anzahlKnoten ; i++){
            if(this.besucht[i] == false) return true;
        }
        return false;
    }
    // Methoden Ende
    // ------------------------------------------ }
}