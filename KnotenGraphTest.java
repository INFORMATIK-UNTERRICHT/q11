

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Die Test-Klasse KnotenGraphTest.
 *
 * @author  (Ihr Name)
 * @version (eine Versionsnummer oder ein Datum)
 */
public class KnotenGraphTest
{
    private Graph_Matrix beispielS113;
    private KnotenGraph knotenGr1;
    private KnotenGraph knotenGr2;
    private KnotenGraph knotenGr3;
    private KnotenGraph knotenGr4;
    private KnotenGraph knotenGr5;
    private KnotenGraph knotenGr6;

    /**
     * Konstruktor fuer die Test-Klasse KnotenGraphTest
     */
    public KnotenGraphTest()
    {
    }

    /**
     *  Setzt das Testgerüst fuer den Test.
     *
     * Wird vor jeder Testfall-Methode aufgerufen.
     */
    @Before
    public void setUp()
    {
        beispielS113 = new Graph_Matrix(6);
        knotenGr1 = new KnotenGraph("0");
        knotenGr2 = new KnotenGraph("1");
        knotenGr3 = new KnotenGraph("2");
        knotenGr4 = new KnotenGraph("3");
        knotenGr5 = new KnotenGraph("4");
        knotenGr6 = new KnotenGraph("5");
        beispielS113.knotenEinfuegen(knotenGr1);
        beispielS113.knotenEinfuegen(knotenGr2);
        beispielS113.knotenEinfuegen(knotenGr3);
        beispielS113.knotenEinfuegen(knotenGr4);
        beispielS113.knotenEinfuegen(knotenGr5);
        beispielS113.knotenEinfuegen(knotenGr6);
        beispielS113.kanteEinfuegen("0", "1", 1);
        beispielS113.kanteEinfuegen("0", "3", 1);
        beispielS113.kanteEinfuegen("0", "4", 1);
        beispielS113.kanteEinfuegen("3", "4", 1);
        beispielS113.kanteEinfuegen("2", "3", 1);
        beispielS113.kanteEinfuegen("4", "2", 1);
        beispielS113.kanteEinfuegen("2", "5", 1);
    }

    /**
     * Gibt das Testgerüst wieder frei.
     *
     * Wird nach jeder Testfall-Methode aufgerufen.
     */
    @After
    public void tearDown()
    {
    }

    @Test
    public void adjazenzenmatrixAusgeben()
    {
        beispielS113.ausgeben(10);
    }

    @Test
    public void fuhereTiefensucheMehrmalsDurch()
    {
        beispielS113.tiefensuche(0);
        beispielS113.tiefensuche(1);
        beispielS113.tiefensuche(2);
    }

    @Test
    public void fuehrePfadsucheMehrmalsAus()
    {
        beispielS113.wegeSuchen(0, 5);
        beispielS113.wegeSuchen("1", "3");
    }
}





